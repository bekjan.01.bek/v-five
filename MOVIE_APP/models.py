from django.db import models
from django.db.models import Avg
from django.utils import timezone

timezone.now()

class Director(models.Model):
    name = models.CharField(max_length=312)

    class Meta:
        verbose_name = 'Режиссер'
        verbose_name_plural = 'Режиссеры'

    def __str__(self):
        return self.name


class Movie(models.Model):
    title = models.CharField('Название', max_length=312)
    description = models.TextField('Описание')
    duration = models.TimeField('Время')
    director = models.ForeignKey(Director, on_delete=models.CASCADE, null=True, related_name='movies')

    class Meta:
        verbose_name = 'Фильм'
        verbose_name_plural = 'Фильмы'

    def __str__(self):
        return self.title


class Review(models.Model):
    STARS_CHOICES = ((i, i * '*') for i in range(1, 6))
    text = models.CharField(max_length=312)
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE, related_name='reviews')
    rating = models.PositiveSmallIntegerField(choices=STARS_CHOICES, default=1)


    class Meta:
        verbose_name = 'Отзыв'
        verbose_name_plural = 'Отзывы'

    def __str__(self):
        return self.text





