from django.urls import path
from . import views

urlpatterns = [
    # GET -> list.  POST -> create
    path('api/v1/directors/', views.DirectorListAPIView.as_view()),
    # GET -> retrieve, PUT -> update, DELETE -> delete
    path('api/v1/directors/<int:pk>/', views.DirectorsDetailAPIView.as_view()),

    path('api/v1/movies/', views.MoviesListAPIView.as_view()),
    path('api/v1/movies/<int:pk>/', views.MoviesDetailAPIView.as_view()),
    path('api/v1/reviews/', views.ReviewListAPIView.as_view()),
    path('api/v1/movies/reviews/', views.MovieReviewAPIView.as_view()),
    path('api/v1/reviews/<int:id>/', views.ReviewDetailAPIView.as_view()),
]