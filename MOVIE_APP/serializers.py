from rest_framework import serializers
from .models import Director, Review, Movie
from django.db.models import Avg

class DirectorSerializer(serializers.ModelSerializer):
    movies_count = serializers.SerializerMethodField()

    class Meta:
        model = Director
        fields = ('id', 'name', 'movies_count')

    def get_movies_count(self, obj):
        return obj.movies.count()


class MoviesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Movie
        fields = ('id', 'title', 'description', 'duration', 'director')


class ReviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Review
        fields = ('id', 'text', 'movie', 'rating',)

class AverageSerializer(serializers.ModelSerializer):
    average_rating = serializers.SerializerMethodField()

    class Meta:
        model = Review
        fields = ('movie', 'text', 'average_rating')

    def get_average_rating(self, obj):
        average_rating = Review.objects.filter(movie=obj.movie).aggregate(avg_rating=Avg('rating'))
        return round(average_rating['avg_rating'], 1) if average_rating['avg_rating'] else 0.0


class DirectorValidateSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=100)


class MovieValidateSerializer(serializers.Serializer):
    title = serializers.CharField(max_length=100)
    description = serializers.CharField()
    duration = serializers.IntegerField()
    director_id = serializers.IntegerField()

    def validate_director_id(self, value: list):
        try:
            Director.objects.get(id=value)
        except Director.DoesNotExist as e:
            raise serializers.ValidationError(str(e))


class ReviewValidateSerializer(serializers.Serializer):
    text = serializers.CharField()
    movie_id = serializers.IntegerField()
    rating = serializers.IntegerField()

    # проверка id
    def validate_movie_id(self, value: list):
        try:
            Movie.objects.get(id=value)
        except Movie.DoesNotExist as e:
            raise serializers.ValidationError(str(e))
        return value

#  проверка на ошибку если это лист и внем несколько id
# def validate_tags(self, value: list):
#     for tags_is in value:
#         try:
#             Tag.objects.get(id=tags_is)
#         except Tag.DoesNotExist as e:
#             raise serializers.ValidationError(str(e))
