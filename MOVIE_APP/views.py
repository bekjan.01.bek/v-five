# from rest_framework.response import Response
# from rest_framework.decorators import api_view
# from django.db.models import Count
# from .serializers import DirectorSerializer, MoviesSerializer, ReviewSerializer, AverageSerializer, DirectorValidateSerializer,\
#     MovieValidateSerializer, ReviewValidateSerializer
# from .models import Director, Movie, Review

from MOVIE_APP.models import Director, Movie, Review

from rest_framework.generics import (
    ListCreateAPIView,
    ListAPIView,
    RetrieveUpdateDestroyAPIView
)
from MOVIE_APP.serializers import (
    DirectorSerializer,
    MoviesSerializer,
    ReviewSerializer,
    AverageSerializer,
)


class DirectorListAPIView(ListCreateAPIView):
    queryset = Director.objects.all()
    serializer_class = DirectorSerializer
    pagination_class = None


class MoviesListAPIView(ListCreateAPIView):
    queryset = Movie.objects.all()
    serializer_class = MoviesSerializer


class ReviewListAPIView(ListCreateAPIView):
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer
    # pagination_class = PageNumberPagination


class MovieReviewAPIView(ListAPIView):
    queryset = Review.objects.all()
    serializer_class = AverageSerializer


class DirectorsDetailAPIView(RetrieveUpdateDestroyAPIView):
    queryset = Director.objects.all()
    serializer_class = DirectorSerializer


class MoviesDetailAPIView(RetrieveUpdateDestroyAPIView):
    queryset = Movie.objects.all()
    serializer_class = MoviesSerializer


class ReviewDetailAPIView(RetrieveUpdateDestroyAPIView):
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer
    lookup_field = 'id'



# @api_view(['GET', 'POST'])
# def director_view(request):
#     if request.method == 'GET':
#         directors = Director.objects.annotate(movies_count=Count('movies'))
#         serializer = DirectorSerializer(instance=directors, many=True)
#         return Response(serializer.data)
#     elif request.method == 'POST':
#         serializer = DirectorValidateSerializer(data=request.data)
#         serializer.is_valid(raise_exception=True)
#         director = Director.objects.create(name=serializer.validated_data.get('name'))
#         serializer = DirectorSerializer(instance=director, many=False)
#         return Response(serializer.data)
#
#
# @api_view(['GET', 'PUT', 'DELETE'])
# def directors_view_id(request, id):
#     try:
#         director = Director.objects.get(id=id)
#     except Director.DoesNotExist:
#         return Response({'ERROR': f"Режиссер с id {id} не существует"})
#
#     if request.method == 'GET':
#         serializer = DirectorSerializer(instance=director)
#         return Response(serializer.data, status=200)
#     elif request.method == 'PUT':
#         serializer = DirectorValidateSerializer(data=request.data)
#         serializer.is_valid(raise_exception=True)
#         director.name = serializer.validated_data.get('name', director.name)
#         director.save()
#         serializer = DirectorSerializer(instance=director)
#         return Response(serializer.data)
#     elif request.method == 'DELETE':
#         director.delete()
#         return Response(status=204)
#
#
# @api_view(['GET', 'POST'])
# def movies_view(request):
#     if request.method == "GET":
#         movies = Movie.objects.all()
#         data = MoviesSerializer(instance=movies, many=True).data
#         return Response(data)
#     elif request.method == "POST":
#         serializer = MovieValidateSerializer(data=request.data)
#         serializer.is_valid(raise_exception=True)
#         movie = Movie.objects.create(
#             title=serializer.validated_data.get('title'),
#             description=serializer.validated_data.get('description'),
#             duration=serializer.validated_data.get('duration'),
#             director_id=serializer.validated_data.get('director_id')
#         )
#         serializer = MoviesSerializer(instance=movie, many=False)
#         return Response(serializer.data)
#
#
# @api_view(['GET', 'PUT', 'DELETE'])
# def movies_view_id(request, movies_id):
#     try:
#         movie = Movie.objects.get(id=movies_id)
#     except Movie.DoesNotExist:
#         return Response({'ERROR': f"Фильм с id {movies_id} не существует"})
#
#     if request.method == "GET":
#         serializer = MoviesSerializer(instance=movie)
#         return Response(serializer.data)
#     elif request.method == "PUT":
#         serializer = MovieValidateSerializer(data=request.data)
#         serializer.is_valid(raise_exception=True)
#         movie.title = serializer.validated_data.get('title', movie.title)
#         movie.description = serializer.validated_data.get('description', movie.description)
#         movie.duration = serializer.validated_data.get('duration', movie.duration)
#         movie.director_id = serializer.validated_data.get('director_id', movie.director_id)
#         movie.save()
#         serializer = MoviesSerializer(instance=movie)
#         return Response(serializer.data)
#     elif request.method == 'DELETE':
#         movie.delete()
#         return Response(status=204)
#
#
# @api_view(['GET', 'POST'])
# def reviews_view(request):
#     if request.method == "GET":
#         reviews = Review.objects.all()
#         serializer = ReviewSerializer(reviews, many=True)
#         return Response(serializer.data)
#     elif request.method == "POST":
#         serializer = ReviewValidateSerializer(data=request.data)
#         serializer.is_valid(raise_exception=True)
#         review = Review.objects.create(
#             text=serializer.validated_data.get('text'),
#             movie_id=serializer.validated_data.get('movie_id'),
#             rating=serializer.validated_data.get('rating')
#         )
#         serializer = ReviewSerializer(instance=review, many=False)
#         return Response(serializer.data)
#
#
# @api_view(['GET', 'PUT', 'DELETE'])
# def reviews_view_id(request, id):
#     try:
#         review = Review.objects.get(id=id)
#     except Review.DoesNotExist:
#         return Response({'ERROR': f" с такой id {id} не существует"})
#
#     if request.method == 'GET':
#         serializer = ReviewSerializer(instance=review)
#         return Response(serializer.data)
#     elif request.method == 'PUT':
#         serializer = ReviewValidateSerializer(data=request.data)
#         serializer.is_valid(raise_exception=True)
#         review.text = serializer.validated_data.get('text', review.text)
#         review.movie_id = serializer.validated_data.get('movie_id', review.movie_id)
#         review.rating = serializer.validated_data.get('rating', review.rating)
#         review.save()
#         serializer = ReviewSerializer(instance=review)
#         return Response(serializer.data)
#     elif request.method == 'DELETE':
#         review.delete()
#         return Response(status=204)
#
#
# @api_view(['GET'])
# def movie_reviews(request):
#     reviews = Review.objects.all()
#     serializer = AverageSerializer(instance=reviews, many=True)
#     return Response(serializer.data)
